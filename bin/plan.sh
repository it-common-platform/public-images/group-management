#!/bin/bash

function cleanup() {
  set +ex
  vault token revoke -self
}

trap cleanup EXIT
set -e

mkdir -p /apps/local/cert
apk add --no-cache vault libcap
setcap cap_ipc_lock= /usr/sbin/vault
export VAULT_TOKEN=$(vault write -field=token auth/code.vt.edu/login role=gitlab-ci-dit-platform-group-management jwt=$VAULT_ID_TOKEN)
export ED_SERVICE=$(vault kv get -format=json dit.platform/ed-service)
echo $ED_SERVICE | jq -r '.data.data["key.pem"]' | grep -v '^$' | base64 -d - > /apps/local/cert/key.pem
echo $ED_SERVICE | jq -r '.data.data["key.crt"]' | grep -v '^$' | base64 -d - > /apps/local/cert/key.crt
echo $ED_SERVICE | jq -r '.data.data["ca.pem"]' | grep -v '^$' | base64 -d - > /apps/local/cert/ca.pem

ls -al /apps/local/cert/

if [ -n "${PAUSE}" ]; then
  echo "sleeping for ${PAUSE} seconds" ; sleep ${PAUSE}
fi

set -x
gitlab-terraform plan
gitlab-terraform plan-json
