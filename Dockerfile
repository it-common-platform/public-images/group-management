FROM code.vt.edu:5005/devcom/terraform-providers/vt-ed-terraform-provider:0.6.2 as vted

FROM registry.gitlab.com/gitlab-org/terraform-images/stable:latest as terraform

FROM alpine:3.18
ENV VAULT_ADDR=https://vault.es.cloud.vt.edu:8200

RUN apk add --no-cache bash jq curl idn2-utils gcompat aws-cli vault

COPY --from=terraform /usr/bin/gitlab-terraform /usr/local/bin/
COPY --from=terraform /usr/local/bin/terraform /usr/local/bin/
COPY --from=vted /terraform-provider-vted-linux-amd64 /root/.terraform.d/plugins/terraform.vt.edu/vt/ed/0.6.2/linux_amd64/terraform-provider-ed

COPY bin /usr/local/bin
RUN chmod 755 /usr/local/bin/validate.sh && chmod 755 /usr/local/bin/apply.sh && chmod 755 /usr/local/bin/plan.sh
